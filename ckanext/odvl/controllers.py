from ckan.plugins import toolkit
from ckan.model.meta import Session
from ckan.common import response
import ckan.lib.base as base
import ckan.model as model
import ckan.logic as logic
import unicodecsv as csv
import ckan.lib.helpers as h
try: from cStringIO import StringIO
except ImportError: from StringIO import StringIO

if toolkit.check_ckan_version(min_version='2.1'):
    BaseController = toolkit.BaseController
else:
    from ckan.lib.base import BaseController

c = base.c

API_DATASET_Q = u'(urls:"/sparql" OR urls:"api/v2" OR res_format:"WFS" OR res_format:"WMS" OR res_format:"WCS" OR res_name:"Esri Rest API" OR res_format:"API")'

from sqlalchemy import or_

class OdvlController(BaseController):

    def summary_facet(self, facet, format):

        context = {'model': model,
                   'user': c.user, 'auth_user_obj': c.userobj}

        logic.check_access('sysadmin', context, {})

        #import ckan.model as model
        #engine = model.meta.engine
        conn = Session.connection()

        facet_title = None
        sql = None

        if facet == 'org':
            facet_title = 'Organization'
            sql = '''
                SELECT grp.name,
                       COUNT(CASE WHEN p.state = 'active' THEN 1 END) as TotalActive,
                       COUNT(CASE WHEN p.private = TRUE AND p.state = 'active' THEN 1 END) as PrivateActive,
                       COUNT(CASE WHEN p.state = 'draft' THEN 1 END) as Draft,
                       COUNT(CASE WHEN p.state = 'deleted' THEN 1 END) as Deleted
                   FROM package AS p
                   LEFT OUTER JOIN public.group AS grp ON grp.id = p.owner_org
                   GROUP BY grp.name
                   ORDER BY TotalActive DESC
            '''
        elif facet == 'user':
            facet_title = 'User'
            sql = '''
                SELECT u.name,
                       COUNT(CASE WHEN p.state = 'active' THEN 1 END) as TotalActive,
                       COUNT(CASE WHEN p.private = TRUE AND p.state = 'active' THEN 1 END) as PrivateActive,
                       COUNT(CASE WHEN p.state = 'draft' THEN 1 END) as Draft,
                       COUNT(CASE WHEN p.state = 'deleted' THEN 1 END) as Deleted
                   FROM package AS p
                   LEFT OUTER JOIN public.user AS u ON u.id = p.creator_user_id
                   GROUP BY u.name
                   ORDER BY TotalActive DESC
            '''
        elif facet == 'theme':
            facet_title = 'Theme'
            sql = '''
                SELECT e.value,
                       COUNT(CASE WHEN p.state = 'active' THEN 1 END) as TotalActive,
                       COUNT(CASE WHEN p.private = TRUE AND p.state = 'active' THEN 1 END) as PrivateActive,
                       COUNT(CASE WHEN p.state = 'draft' THEN 1 END) as Draft,
                       COUNT(CASE WHEN p.state = 'deleted' THEN 1 END) as Deleted
                   FROM package AS p
                   INNER JOIN public.package_extra AS e ON e.package_id = p.id
                   WHERE e.key = 'theme'
                   GROUP BY e.value
                   ORDER BY TotalActive DESC
            '''
        elif facet == 'publisher':
            facet_title = 'Publisher'
            sql = '''
                SELECT e.value,
                       COUNT(CASE WHEN p.state = 'active' THEN 1 END) as TotalActive,
                       COUNT(CASE WHEN p.private = TRUE AND p.state = 'active' THEN 1 END) as PrivateActive,
                       COUNT(CASE WHEN p.state = 'draft' THEN 1 END) as Draft,
                       COUNT(CASE WHEN p.state = 'deleted' THEN 1 END) as Deleted
                   FROM package AS p
                   INNER JOIN public.package_extra AS e ON e.package_id = p.id
                   WHERE e.key = 'publisher_name'
                   GROUP BY e.value
                   ORDER BY TotalActive DESC
            '''
        elif facet == 'tag':
            facet_title = 'Tag'
            sql = '''
             SELECT t.name,
                       COUNT(CASE WHEN p.state = 'active' THEN 1 END) as TotalActive,
                       COUNT(CASE WHEN p.private = TRUE AND p.state = 'active' THEN 1 END) as PrivateActive,
                       COUNT(CASE WHEN p.state = 'draft' THEN 1 END) as Draft,
                       COUNT(CASE WHEN p.state = 'deleted' THEN 1 END) as Deleted
                   FROM package AS p
                   INNER JOIN package_tag AS pt ON pt.package_id = p.id
                   INNER JOIN tag AS t ON pt.tag_id = t.id
                   WHERE pt.state = 'active'
                   GROUP BY t.name
                   ORDER BY TotalActive DESC
            '''
        elif facet == 'extra':
            facet_title = 'Extra'
            sql = '''
                SELECT e.key,
                       COUNT(CASE WHEN p.state = 'active' THEN 1 END) as TotalActive,
                       COUNT(CASE WHEN p.private = TRUE AND p.state = 'active' THEN 1 END) as PrivateActive,
                       COUNT(CASE WHEN p.state = 'draft' THEN 1 END) as Draft,
                       COUNT(CASE WHEN p.state = 'deleted' THEN 1 END) as Deleted
                   FROM package AS p
                   INNER JOIN public.package_extra AS e ON e.package_id = p.id
                   GROUP BY e.key
                   ORDER BY TotalActive DESC
            '''
        elif facet == 'license':
            facet_title = 'License'
            sql = '''
                SELECT p.license_id,
                       COUNT(CASE WHEN p.state = 'active' THEN 1 END) as TotalActive,
                       COUNT(CASE WHEN p.private = TRUE AND p.state = 'active' THEN 1 END) as PrivateActive,
                       COUNT(CASE WHEN p.state = 'draft' THEN 1 END) as Draft,
                       COUNT(CASE WHEN p.state = 'deleted' THEN 1 END) as Deleted
                   FROM package AS p
                   GROUP BY p.license_id
                   ORDER BY TotalActive DESC
            '''

        csvout = StringIO()
        csvwriter = csv.writer(
            csvout,
            quoting=csv.QUOTE_NONNUMERIC,
        )

        csvwriter.writerow([facet_title, 'Total Active', 'Private Active', 'Draft', 'Deleted'])

        for t in conn.execute(sql).fetchall():
            csvwriter.writerow(t)

        csvout.seek(0)

        response.headers['Content-Type'] = 'text/csv'

        return csvout.read()

    def summary_csv(self):

        context = {'model': model,
                   'user': c.user, 'auth_user_obj': c.userobj}

        logic.check_access('sysadmin', context, {})

        #import ckan.model as model
        #engine = model.meta.engine
        conn = Session.connection()

        old_sql = '''
            SELECT DISTINCT p.id,
                   p.name,
                   u.name,
                   grp.name,
                   p.metadata_modified,
                   p.private,
                   hs.title,
                   ( SELECT bool_or(ho.current) FROM harvest_object AS ho WHERE ho.package_id = p.id GROUP BY ho.package_id) AS isCurrent,
                   p.state
               FROM package AS p
               LEFT OUTER JOIN public.user AS u ON u.id = p.creator_user_id
               LEFT OUTER JOIN public.group AS grp ON grp.id = p.owner_org
               LEFT OUTER JOIN harvest_object AS ho ON ho.package_id = p.id
               LEFT OUTER JOIN harvest_source AS hs ON ho.harvest_source_id = hs.id
               ORDER BY grp.name, p.metadata_modified
        '''

        sql = '''
SELECT DISTINCT p.id,
                   p.name,
                   u.name,
                   grp.name,
                   p.metadata_modified,
                   p.metadata_created,
                   ( SELECT extras.value FROM package_extra as extras WHERE extras.package_id = p.id AND extras.key = 'publisher_name') AS publisher_name,
                   ( SELECT extras.value FROM package_extra as extras WHERE extras.package_id = p.id AND extras.key = 'publisher_email') AS publisher_email,
                   ( SELECT extras.value FROM package_extra as extras WHERE extras.package_id = p.id AND extras.key = 'total_owner') AS total_owner,
                   ( SELECT extras.value FROM package_extra as extras WHERE extras.package_id = p.id AND extras.key = 'total_publisher') AS total_publisher,
                   ( SELECT extras.value FROM package_extra as extras WHERE extras.package_id = p.id AND extras.key = 'total_custodian') AS total_custodian,
                   p.private,
                   p.state,
                   count(ho.id)
               FROM package AS p
               LEFT OUTER JOIN public.user AS u ON u.id = p.creator_user_id
               LEFT OUTER JOIN public.group AS grp ON grp.id = p.owner_org
               LEFT OUTER JOIN public.harvest_object AS ho ON ho.package_id = p.id
               GROUP BY p.id,
                   p.name,
                   u.name,
                   grp.name,
                   p.metadata_modified,
                   p.metadata_created,
                   p.private,
                   p.state
               ORDER BY grp.name, p.metadata_modified
        '''

        csvout = StringIO()
        csvwriter = csv.writer(
            csvout,
            quoting=csv.QUOTE_NONNUMERIC
        )

        csvwriter.writerow(['ID', 'name', 'creator', 'org', 'modification date', 'creation date', 'publisher name', 'publisher email', 'total_owner', 'total_publisher', 'total_custodian', 'private?', 'state', 'harvestHistory'])

        for t in conn.execute(sql).fetchall():
            csvwriter.writerow(t)

        csvout.seek(0)

        response.headers['Content-Type'] = 'text/csv'

        return csvout.read()


    def api_datasets(self):

        context = {'model': model,
                   'user': c.user, 'auth_user_obj': c.userobj, 'for_view': True}

        logic.check_access('sysadmin', context, {})

        query_dict = {
            #'sort': 'score desc, metadata_modified desc',
            #'facet.field': [u'organization', u'groups', u'tags', u'res_format', u'license_id'],
            #'fq_list': ['+dataset_type:dataset', '-dataset_type:harvest'],
            'q': API_DATASET_Q,
            'rows': 10000,
            'start': 0,
            'include_private': True}
        query = logic.get_action('package_search')(context, query_dict)

        datasets = query['results']


        csvout = StringIO()
        csvwriter = csv.writer(csvout,quoting=csv.QUOTE_NONNUMERIC)

        csvwriter.writerow(['dataset_name', 'dataset_id', 'org', 'publisher', 'Private', 'State', 'ckan_url'])

        for d in datasets:
            ckan_url = h.url_for(controller='package',
                                 action='read',
                                 id=d['id'],
                                 qualified=True)
            csvwriter.writerow([
                d['name'],
                d['id'],
                d.get('organization',{}).get('name',''),
                next((extra["value"] for extra in d.get('extras', []) if extra["key"] == "publisher_name"), ''),
                d.get('private', False),
                d.get('state', ''),
                ckan_url
            ])

        csvout.seek(0)

        response.headers['Content-Type'] = 'text/csv'

        return csvout.read()


    def api_resources(self):

        context = {'model': model,
                   'user': c.user, 'auth_user_obj': c.userobj, 'search_query': True}

        logic.check_access('sysadmin', context, {})

        q = model.Session.query(model.Resource) \
            .join(model.Package) \
            .filter(model.Package.state == 'active') \
            .filter(model.Package.private == False) \
            .filter(model.Resource.state == 'active')

        url_attr = getattr(model.Resource, 'url')
        format_attr = getattr(model.Resource, 'format')
        name_attr = getattr(model.Resource, 'name')
        q = q.filter(or_(
            url_attr.ilike('%' + unicode('api/v2') + '%'),
            format_attr.ilike('%' + unicode('wms') + '%'),
            format_attr.ilike('%' + unicode('wfs') + '%'),
            format_attr.ilike('%' + unicode('wcs') + '%'),
            format_attr.ilike('%' + unicode('api') + '%'),
            name_attr.ilike('%' + unicode('Esri Rest API') + '%')
        ))
        q = q.order_by(getattr(model.Package, 'owner_org'))
        q = q.order_by(getattr(model.Package, 'name'))

        resource_list = []

        count = q.count()

        csvout = StringIO()
        csvwriter = csv.writer(csvout,quoting=csv.QUOTE_NONNUMERIC)

        csvwriter.writerow(['resource_name', 'resource_id', 'dataset_name', 'dataset_id', 'format', 'publisher', 'Private', 'State', 'ckan_url', 'url'])

        for r in q.all():
            ckan_url = h.url_for(controller='package',
                            action='resource_read',
                            id=r.package.id,
                            resource_id=r.id,
                            qualified=True)
            csvwriter.writerow([
                r.name,
                r.id,
                r.package.name,
                r.package.id,
                r.format,
                r.package.extras.get('publisher_name',''),
                r.package.private,
                r.package.state,
                ckan_url,
                r.url
            ])

        csvout.seek(0)

        response.headers['Content-Type'] = 'text/csv'

        return csvout.read()

